# Str To Int Arr

## What is this

This lets you turn strings into arrays of ints.

For example:

```
0..5 	-> {0, 1, 2, 3, 4}
0...5 	-> {0, 1, 2, 3, 4}
0,5   	-> {0, 5}
```

At one point ".." was "exclusive" and "..." was "inclusive".
That meant "..." included the 5 and ".." didn't.

However those halcyon days are over. "..", "..." and "..........." now
do the same thing: return an inclusive list. I need to put it back the
way it was. I took it out, since I was incompetent. If you step into
the labyrinth that is this lib's code you will see that. It works,
though.

Spaces don't matter:

```
0             .. 5
```

It's meant to be convenient:

```
...         -> {0, 1, 2, 3, 4, 5}
..          -> {0, 1, 2, 3, 4}
```

Those assumes you've said your max is 5. You have to tell the function
what the max value is.

You can also:

```
3...      	-> {3, 4, 5}
...3 		-> {0, 1, 2, 3}
```

And, finally, you can:

```
4...,2 		-> {4, 0, 1, 2, 3, 4, 5, 2}
4..,2 		-> {4, 0, 1, 2, 3, 4, 2}
4,..2 		-> {4, 0, 1}
4,...2 		-> {4, 0, 1, 2}
```

It doesn't optionally remove dups but I expect it will before long.

## Types

```
struct s2ia_info {
	const char *parse_end;
	enum s2ia_errno s2ia_errno;
};
```

* parse\_end is where the parsing stopped, if there's was an error.
* s2ia\_errno is an error code. There's a s2ia\_strerror function
  that'll give you the error as a string. You don't have to free it.

## Functions

### s2ia\_str\_to\_int\_arr

Produces an array of ints from your string.

#### Args

* Returns: an s2ia\_info.
* int \*\*res: the ints themselves.
* size\_t \*n\_res: number of  ints themselves.
* const char \*s: The start of the string you're parsing.
* const char \*e: The place to stop parsing, or NULL if you want to
  stop parsing at \0.
* int max: the highest value you're allowing in  the arr.
* flags: flags to affect how the string is parsed: see below.

### s2ia\_str\_to\_relative\_int\_arr

Produces an array of ints from your string and an array. Say you
give it the string "0..2" and the arr {0, 1, 2, 3}. You will get,
actually, {0, 1, 2}, same as you'd get from the first function. But
if the arr was {0, 3, 10}, you'd get {0, 3, 10}.

I'm being honest. I can't remember what the point was. I suppose it's
to save you bothering  with an extra level of indirection.

* Returns: an s2ia\_info.
* int \*\*res: the ints themselves.
* size\_t \*n\_res: number of ints themselves.
* const char \*s: The start of the string you're parsing.
* const char \*e: The place to stop parsing, or NULL if you want to
  stop parsing at \0.
* int \*original\_arr: the array you're working from.
* size\_t n\_original\_arr. The number of elements in that array.
* flags: flags to affect how the string is parsed: see below.

## Flags

* S2IA\_FLAG\_NO\_ERROR\_BAD\_TOKEN. What it sounds like. It's useful
  if you're parsing a longer string.
* S2IA\_FLAG\_ALLOW\_OUT\_OF\_RANGE. Badly named. This will clamp ints
  between 0 and  max, instead  of erroring.

## How to get it

```
git clone 'https://gitlab.com/objection/str-to-int-arr.git
```

## Install

```
meson  build
ninja -Cbuild install
```

This means you'll need Meson, which you can  get from  your package
manager.

## Don't install

However, you don't need to install. It's just a .c and a .h file.
