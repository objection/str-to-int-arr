#pragma once

#include <stddef.h>
#include <unistd.h>

#define S2IA_ERRORS \
	x(S2IA_ERROR_NO_ERROR, "No error") \
	x(S2IA_OUT_OF_MEMORY, "Out of memory") \
	x(S2IA_ERROR_BAD_CHAR, "Unrecognised char") \
	x(S2IA_ERROR_NUM_TOO_BIG, "You're trying to index past the max") \
	x(S2IA_ERROR_EXPECTED_NUMBER, "Expected number") \
	x(S2IA_ERROR_EXPECTED_RANGE, "Expected \"..\"") \
	x(S2IA_ERROR_MINUS_NUM_TAKEN_BELOW_ZERO, "You're trying to index below zero") \
	x(N_S2IA_ERROR, NULL)

#define x(a, b) a,
enum s2ia_status {S2IA_ERRORS};
#undef x

extern char *s2ia_error_strs[N_S2IA_ERROR + 1];

enum s2ia_flags {
	S2IA_FLAG_NO_ERROR_BAD_TOKEN = 1 << 0,
	S2IA_FLAG_ALLOW_OUT_OF_RANGE = 1 << 1,
};

struct s2ia_info {
	const char *parse_end;
	enum s2ia_status status;
};

struct s2ia_info s2ia_str_to_int_arr (int **res, ssize_t *n_res, const char *s,
		const char *e, int max, enum s2ia_flags flags);
struct s2ia_info s2ia_str_to_relative_int_arr (int **res, ssize_t *n_res,
		const char *s, const char *e,  int *original_arr, ssize_t n_original_arr,
		enum s2ia_flags flags);
char *s2ia_strerror (enum s2ia_status status);
