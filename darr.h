
#pragma once
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

// Stolen from Rosetta Code, I think, changed here and there.

#define DECLARE_STATIC_ARR(type, name, size) \
	struct name##_sarr_##size { \
		type d[size]; \
		size_t n; \
	}

#define DECLARE_ARR(type, name) \
		typedef struct name##_arr { \
			type *d; \
			size_t a, n; \
		} name##_arr; \
		name##_arr s2ia_create_##name##_arr (size_t init_size); \
		int s2ia_push_##name##_arr (name##_arr *s, type item); \
		type s2ia_pop_##name##_arr (name##_arr *s, bool error_on_stack_empty); \
		type s2ia_peek_##name##_arr (name##_arr *s); \
		int s2ia_insert_##name##_arr (name##_arr *s, type *what, size_t where, \
				size_t n); \
		void s2ia_remove_from_##name##_arr (name##_arr *s, size_t where, \
				size_t n); \
		void s2ia_delete_##name##_arr(name##_arr *s)

#define DEFINE_ARR(type, name) \
		name##_arr s2ia_create_##name##_arr(size_t init_size) { \
			name##_arr res; \
			if (!init_size) init_size = 4; \
			res.d = malloc (sizeof(type) * init_size); \
			if (!res.d) { \
				free (res.d); \
				res.a = -1; \
			} else { \
				res.n = 0, res.a = init_size; \
			} \
			return res; \
		} \
		int s2ia_push_##name##_arr(name##_arr *s, type item) {\
			type *tmp; \
			if ((*s).n >= (*s).a) { \
				tmp = realloc ((*s).d, ((*s).a * 2) * sizeof (type)); \
				if (!tmp) return -1;  \
				(*s).d = tmp; \
				(*s).a *= 2; \
			} \
			(*s).d[(*s).n++] = item; \
			return (*s).n; \
		} \
		type s2ia_pop_##name##_arr(name##_arr *s, bool error_on_stack_empty) { \
			type tmp = {0}; \
			if (!(*s).n) { \
				if (error_on_stack_empty) abort(); \
				else return tmp; \
			} \
			tmp = (*s).d[--(*s).n]; \
			/* if ((*s).n * 2 <= (*s).a && (*s).a >= 8) { \ */ \
			/* 	(*s).a /= 2; \ */ \
			/* 	(*s).d = realloc ((*s).d, (*s).a * sizeof (type)); \ */ \
			/* } \ */ \
			return tmp; \
		} \
		int s2ia_insert_##name##_arr (name##_arr *s, type *what, size_t where, \
				size_t n) { \
			if (where > (*s).n || where < 0) return 1; \
			if ((*s).n + n >= (*s).a) { \
				(*s).a += n + 1;  \
				(*s).a *= 2; \
				type *tmp = realloc ((*s).d, (*s).a * sizeof *(*s).d); \
				if (!tmp) return 2; \
				(*s).d = tmp; \
			} \
			memmove ((*s).d + where + n, (*s).d + where, \
					((*s).n - where) *  sizeof *(*s).d); \
			memcpy ((*s).d + where, what, n * sizeof *(*s).d); \
			(*s).n += n; \
			return 0; \
		} \
		int s2ia_insert_##name##_arr_before (name##_arr *s, type *what, size_t where, \
				size_t n) { \
			if (where > (*s).n || where < 0) return 1; \
			if ((*s).n + n >= (*s).a) { \
				(*s).a += n + 1;  \
				(*s).a *= 2; \
				type *tmp = realloc ((*s).d, (*s).a * sizeof *(*s).d); \
				if (!tmp) return 2; \
				(*s).d = tmp; \
			} \
			memmove ((*s).d + where + n, (*s).d + where, \
					((*s).n - where) *  sizeof *(*s).d); \
			memcpy ((*s).d + where, what, n * sizeof *(*s).d); \
			(*s).n += n; \
			return 0; \
		} \
		void s2ia_remove_from_##name##_arr (name##_arr *s, size_t where, \
				size_t _n) { \
			 memmove ((*s).d + where, (*s).d + where + _n, \
					 ((*s).n - where - _n) * sizeof *(*s).d); \
			(*s).n -= _n; \
		} \
		void s2ia_delete_##name##_arr(name##_arr *s) { \
			if ((*s).n) free ((*s).d); \
			(*s).n = (*s).a = 0; \
		} \
		type s2ia_peek_##name##_arr (name##_arr *s) { \
			return (*s).d[(*s).n - 1]; \
		}
#if 0
#define arr_empty(s) (!(s).n)
#define arr_size(s) ((s).n)
#endif

// NOTE: Leave these here, unless you want to enter linking hell
DECLARE_ARR (int, int);
