#define _GNU_SOURCE

#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <stdbool.h>
#include "str-to-int-arr.h"

struct int_arr {
	int *d;
	ssize_t n, a;
};

static int int_arr_push (struct int_arr *arr, int the_int) {
	if (arr->n >= arr->a) {
		arr->a *= 2;
		int *new = realloc (arr->d, arr->a * sizeof *arr->d);
		if (!new) return  1;
		arr->d = new;
	}
	arr->d[arr->n++] = the_int;
	return 0;
}

static int int_arr_create (struct int_arr *r) {
	r->a = 64;
	r->n = 0;
	r->d = malloc (r->a * sizeof *r->d);
	if (!r->d) return 1;
	return 0;
}

char *s2ia_error_strs[N_S2IA_ERROR + 1];

enum s2ia_kind {
	S2IA_KIND_DIGITS,
	S2IA_KIND_COMMA,
	S2IA_KIND_RANGE,
	S2IA_KIND_END,
	S2IA_KIND_ERROR,
};

struct s2ia_token {
	const char *s, *e;
	enum s2ia_kind kind;
} s2ia_t;

struct s2ia_info s2ia_info;

static void s2ia_get_token (const char *e) {

	while (s2ia_t.e < e && isspace (*s2ia_t.e)) s2ia_t.e++;
	s2ia_t.s = s2ia_t.e;
	if (s2ia_t.s == e) {
		s2ia_t.e = s2ia_t.s;
		s2ia_t.kind = S2IA_KIND_END;
		return;
	}
	s2ia_t.e = s2ia_t.s + 1;
	switch (*s2ia_t.s) {
		case '-':
			s2ia_t.e++; // fall-through
		case '0' ... '9':
			while (s2ia_t.e < e && isdigit (*s2ia_t.e)) s2ia_t.e++;
			s2ia_t.kind = S2IA_KIND_DIGITS;
			break;
		case ',':
			s2ia_t.kind = S2IA_KIND_COMMA;
			break;
		case '.':
			if (*s2ia_t.e == '.') {
				s2ia_t.kind = S2IA_KIND_RANGE;
				while (s2ia_t.e < e && *s2ia_t.e == '.')
					s2ia_t.e++;
			} else
				s2ia_t.kind = S2IA_KIND_ERROR;
			break;
		default:
			s2ia_t.kind = S2IA_KIND_ERROR;
			break;
	}
}

#define x(a, b) b,
char *s2ia_error_strs[N_S2IA_ERROR + 1] = {S2IA_ERRORS};
#undef x

static void s2ia_make_info (enum s2ia_status status) {
	s2ia_info = (struct s2ia_info) {.parse_end = s2ia_t.s,
		.status = status};
}

static int s2ia_get_int (int *r, enum s2ia_flags flags) {
	(void) flags;
	errno = 0;
	char *endptr;
	*r = strtol (s2ia_t.s, &endptr, 0);

	// This is bad error checking
	if (errno || endptr == s2ia_t.s) {
		s2ia_make_info (S2IA_ERROR_EXPECTED_NUMBER);
		return 1;
	}
	return 0;
}

static int s2ia_normalise_int (int *the_int, int max, enum s2ia_flags flags) {
	if (*the_int < 0) *the_int = max + *the_int;
	if (*the_int < 0) {
		if (flags & S2IA_FLAG_ALLOW_OUT_OF_RANGE)
			*the_int = 0;
		else {
			s2ia_make_info (S2IA_ERROR_MINUS_NUM_TAKEN_BELOW_ZERO);
			return 1;
		}
	}
	if (*the_int >= max) {
		if (flags & S2IA_FLAG_ALLOW_OUT_OF_RANGE)
			*the_int = max - 1;
		else {
			s2ia_make_info (S2IA_ERROR_NUM_TOO_BIG);
			return 1;
		}
	};
	return 0;
}

static int s2ia_parse_range (struct int_arr *arr, int max, const char *e,
		enum s2ia_flags flags) {
	s2ia_get_token (e);
	int end_val;
	if ((flags & S2IA_FLAG_NO_ERROR_BAD_TOKEN && s2ia_t.kind == S2IA_KIND_ERROR)
			|| (s2ia_t.kind == S2IA_KIND_END || s2ia_t.kind == S2IA_KIND_COMMA))
		end_val = max - 1;
	else {
		if (s2ia_t.kind == S2IA_KIND_ERROR) {
			s2ia_make_info (S2IA_ERROR_BAD_CHAR);
			return 1;
		}

		if (s2ia_t.kind == S2IA_KIND_DIGITS) {
			if (s2ia_get_int (&end_val, flags))
				return 1;
			if (s2ia_normalise_int (&end_val, max, flags))
				return 1;
		} else end_val = max;
	}

	int the_int = arr->d[arr->n - 1];

	if (end_val < 0) end_val = max + end_val;

	if (the_int < end_val) {
		while (the_int < end_val) {
			the_int++;
			int_arr_push (arr, the_int);
		}
	} else {
		while (the_int > end_val) {
			the_int--;
			int_arr_push (arr, the_int);
		}
	}

	return 0;
}

static bool s2ia_is_end (enum s2ia_flags flags) {
	if (s2ia_t.kind == S2IA_KIND_END) return 1;
	if (flags & S2IA_FLAG_NO_ERROR_BAD_TOKEN
			&& s2ia_t.kind == S2IA_KIND_ERROR) return 1;
	return 0;
}

struct s2ia_info s2ia_str_to_int_arr (int **r, ssize_t *n_pr, const char *s,
		const char *e, int max, enum s2ia_flags flags) {

	struct int_arr arr;
	if (int_arr_create (&arr))
		return (struct s2ia_info) {.status = S2IA_OUT_OF_MEMORY};

	s2ia_info = (struct s2ia_info) {};
	if (!e) e = strchr (s, '\0');

	int the_int = 0;
	s2ia_t.e = s;
	s2ia_get_token (e);
	for (;;) {

		if (s2ia_t.kind == S2IA_KIND_RANGE)
			int_arr_push (&arr, 0);
		else {
			if (s2ia_t.kind == S2IA_KIND_DIGITS) {
				if (s2ia_get_int (&the_int, flags)) break;
				if (s2ia_normalise_int (&the_int, max, flags)) break;
			} else {
				s2ia_make_info (S2IA_ERROR_EXPECTED_NUMBER);
				break;
			}
			s2ia_get_token (e);
			int_arr_push (&arr, the_int);
		}

		if (s2ia_is_end (flags)) break;
		if (s2ia_t.kind == S2IA_KIND_ERROR) {
			s2ia_make_info (S2IA_ERROR_BAD_CHAR);
			break;
		}

		if (s2ia_t.kind == S2IA_KIND_COMMA) {
			s2ia_get_token (e);
			continue;
		}
		if (s2ia_t.kind == S2IA_KIND_RANGE) {
			if (s2ia_parse_range (&arr, max, e, flags)) break;
		}
		s2ia_get_token (e);
		if (s2ia_t.kind == S2IA_KIND_COMMA) {
			s2ia_get_token (e);
			continue;
		}

		if (s2ia_is_end (flags)) break;
		if (s2ia_t.kind == S2IA_KIND_ERROR) {
			s2ia_make_info (S2IA_ERROR_BAD_CHAR);
			break;
		}
	}
	s2ia_info.parse_end = s2ia_t.s;
	*n_pr = arr.n;
	*r = arr.d;
	return s2ia_info;
}

struct s2ia_info s2ia_str_to_relative_int_arr (int **pr, ssize_t *n_pr,
		const char *s, const char *e,  int *original_arr,
		ssize_t n_original_arr, enum s2ia_flags flags) {

	if (!e) e = strchr (s, '\0');

	struct int_arr arr = {};
	s2ia_info = s2ia_str_to_int_arr (&arr.d, &arr.n, s, e, n_original_arr, flags);
	if (s2ia_info.status)
		goto fail;

	struct int_arr res_arr;
	int_arr_create (&res_arr);
	for (int *p = arr.d; p - arr.d < arr.n; p++) {
		if (s2ia_normalise_int (p, n_original_arr, flags))
			break;
		int_arr_push (&res_arr, original_arr[*p]);
	}
	*n_pr = res_arr.n;
	*pr = res_arr.d;

fail:
	free (arr.d);
	return s2ia_info;
}
